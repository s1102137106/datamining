﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataMiling
{
    //Read Files Class
    public class FileReader : AFile
    {

        public FileReader(String path)
        {
            this.path = path;
        }

     
        public override void openFile()
        {
            fsSource = new FileStream(path, FileMode.Open, FileAccess.Read);
        }

        public override void closeFile()
        { 
            fsSource.Close();
        }

        //換算傳回一個整數
        public override int readInt()
        { 
            int result=fsSource.ReadByte();
            if(result==-1)
            {
                return -1;
            }
            result+=fsSource.ReadByte()*256;
            result += fsSource.ReadByte() * 65536;
            result += fsSource.ReadByte() * 16777216;

            return result;
        }

        //Read Next Trancation Data
        public override int[] getNextData()
        {
            if (this.readInt() == -1)
            {
                return null;
            }
            this.readInt();
            int count = this.readInt();
            int[] list = new int[count];
            for (int i = 0; i < count; i++)
            {
                list[i] = this.readInt();
            }
            return list;
        }

    }
}
