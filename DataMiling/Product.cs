﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMiling
{
    public class Product
    {
        const string WIRTE_FILE_PATH = @"C:\Users\joenice\Desktop\資料探勘作業\Record\T15I7N0_RECORD.5KD1K.txt";//寫入檔案路徑

        /// <summary>
        /// 下一個產品組合
        /// </summary>
        public Dictionary<int, Product> nextProductNode { get; set; }

        /// <summary>
        /// 上一個產品
        /// </summary>
        public Product preProductNode { get; set; }

        /// <summary>
        /// 產品組合數量(出現過幾次)
        /// </summary>
        public int count { get; set; }

        /// <summary>
        /// 此產品編號
        /// </summary>
        public int productID { get; set; }

        /// <summary>
        /// 建構元 初始化此產品ID
        /// </summary>
        /// <param name="productID"></param>
        public Product(int productID)
        {
            this.productID = productID;
            this.count = 1;
        }


        public void addNode(int productId)
        {
            Product tempNode = null;

            if (this.nextProductNode == null)
            {
                this.nextProductNode = new Dictionary<int, Product>();
            }

            if (!this.nextProductNode.TryGetValue(productId, out tempNode))
            {
                this.nextProductNode.Add(productId, new Product(productId));//Dictionary初始化
            }
            else
            {
                //產品組合已存在那就count++
                tempNode.count++;
            }
        }


        public void show(int depth)
        {
            if (depth == 0)
            {
                return;
            }
            Console.Write("(lv{0} id-{1}), ", depth, this.productID);


            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(WIRTE_FILE_PATH, true))
            {
                file.Write("(lv{0} id-{1}), ", depth, this.productID);
            }

            if (this.preProductNode != null)
            {
                this.preProductNode.show(depth - 1);
            }

        }

        public void showUpDown(int depth)
        {
            //Console.WriteLine("{0} id-{1}", new String('-', depth * 2), this.productID);
            Console.Write("(lv{0} id-{1}), ", depth, this.productID);

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(WIRTE_FILE_PATH))
            {
                file.WriteLine("(lv{0} id-{1}), ", depth, this.productID);
            }

            if (this.nextProductNode != null && this.nextProductNode.Count != 0)
            {
                foreach (Product product in this.nextProductNode.Values)
                {
                    product.showUpDown(depth + 1);
                }
            }
            else
            {
                Console.WriteLine(" Count:{0}", this.count);
            }
        }
    }
}
