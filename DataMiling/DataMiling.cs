﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMiling
{
    /// <summary>
    /// 文字檔
    /// </summary>
    class DataMiling
    {
        static DateTime startTime;//起始時間
        static long comTime;//完成程式耗費時間
        static Dictionary<int, Product> productRoot = new Dictionary<int, Product>();//產品組合

        static List<long> frqitem = new List<long>();//每個組合的item數量加總

        static int OVER_COUNT = 0;//超過此數字的產品組合 ; 條件
        static string FILE_PATH = @"C:\Users\joenice\Desktop\資料探勘作業\testFile\T10I10N0.1KD1K.txt";//檔案路徑
        static string WIRTE_FILE_PATH = @"C:\Users\joenice\Desktop\資料探勘作業\Record\T12I30N0.5KD100K.data";//寫入檔案路徑

        /// <summary>
        /// 主程式(程式進入點)
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //Console.WriteLine("輸入開啟檔案路徑:");
            //FILE_PATH = Console.ReadLine();

            //Console.WriteLine("輸入寫入檔案路徑:");
            //FILE_PATH = Console.ReadLine();
            Console.WriteLine("輸入sup值:");
            OVER_COUNT = Convert.ToInt16(Console.ReadLine());

            //開始計算程式執行時間
            startTime = DateTime.Now;

            //紀錄產品組合
            RecordProduct();

            //RecordRowProduct(new int[] { 0, 0, 0,0 }, new string[] { "1", "2", "3", "4", "5", "6", "7" }, 4, 0);

            //印出答案
            int index = 1;
            foreach (int frq in frqitem)
            {
                if (frq != 0)
                {
                    Console.WriteLine("L{0}:{1}", index, frq);
                }
                index++;
            }
            Console.WriteLine("總數frq:{0}", frqitem.Sum());

            //計算程式執行時間
            comTime = DateTime.Now.Ticks - startTime.Ticks;
            Console.WriteLine("總共費時{0}毫秒 耗時{1}秒", comTime / 10000, comTime / 10000000);

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(WIRTE_FILE_PATH, true))
            {
                file.WriteLine("總共費時{0}毫秒 耗時{1}秒", comTime / 10000, comTime / 10000000);

                file.WriteLine("log:{0}", DateTime.Now.ToString());
            }

            Console.ReadKey();
        }

        /// <summary>
        /// 開始紀錄產品組合， 每一階層就掃描檔案一次 ， 產品組合需大於over才會被留下
        /// </summary>
        /// <param name="nowLevel">目前計算的產品組合階層(EX:2代表兩個產品組合)</param>
        static public void RecordProduct()
        {
            System.IO.StreamReader file;
            int nowLevel = 1;
            bool isBinary = FILE_PATH.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries).Last().Equals("txt");
            //計算所有可能的組合
            while (true)
            {
                if (nowLevel == 3)
                {
                    var a = 1;
                }
                //開檔案
                if (isBinary)
                {
                    //開啟純文字
                    file = new System.IO.StreamReader(FILE_PATH);

                    #region 建立每個產品的Dictionary-純文字

                    string line;
                    string[] productIDs;//每筆產品編號
                    int[] int_productIDs;//每筆產品編號
                    while ((line = file.ReadLine()) != null)
                    {
                        productIDs = line.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                        int_productIDs = new int[productIDs.Length];

                        for (int i = 0; i < productIDs.Length; i++)
                        {
                            int_productIDs[i] = Convert.ToInt32(productIDs[i]);
                        }

                        //第一層特殊處理
                        if (nowLevel == 1)
                        {
                            foreach (int temp in int_productIDs)
                            {
                                Product root = null;
                                if (!productRoot.TryGetValue(temp, out root))
                                {
                                    Product tempRoot = new Product(temp);

                                    productRoot.Add(temp, tempRoot);//初始化
                                }
                                else
                                {
                                    //產品重複出現時 數量+1
                                    root.count++;
                                }
                            }
                        }
                        else
                        {// nowLevel>=2
                            //宣告節點的路徑變化 EX:LEVEL = 3時,應該往下抓取兩階
                            int[] path = new int[nowLevel - 1];
                            for (int i = 0; i < path.Length; i++)
                            {
                                path[i] = 0;
                            }
                            RecordRowProduct(path, int_productIDs, nowLevel, 0);
                        }
                    }
                    #endregion

                    //關閉檔案
                    file.Close();
                }
                else
                {
                    //開啟二進位
                    //開檔案
                    AFile file2 = new FileReader(FILE_PATH);
                    file2.openFile();


                    #region 建立每個產品的Dictionary
                    int[] productIDs;
                    int temp;
                    while ((productIDs = file2.getNextData()) != null)
                    {
                        for (int m = 0; m < productIDs.Length; m++)
                        {
                            temp = productIDs[m];
                            if (nowLevel == 1)
                            {
                                Product root = null;
                                if (!productRoot.TryGetValue(temp, out root))
                                {
                                    Product tempRoot = new Product(temp);

                                    productRoot.Add(temp, tempRoot);//初始化
                                }
                                else
                                {
                                    //產品重複出現時 數量+1
                                    root.count++;
                                }
                            }
                            else
                            {
                                int[] path = new int[nowLevel - 1];
                                for (int i = 0; i < path.Length; i++)
                                {
                                    path[i] = 0;
                                }
                                RecordRowProduct(path, productIDs.ToArray(), nowLevel, 0);
                            }


                        }
                    }


                    #endregion
                    file2.closeFile();
                }






                //刪除小於需求數量的產品 (刪除當前階層即可)
                DelProduct(productRoot, nowLevel, 1);

                //加總Frq
                frqitem.Add(CountFrq(1, nowLevel, productRoot));

                if (productRoot.Count == 0)
                {
                    break;
                }
                nowLevel = nowLevel + 1;
                Console.WriteLine(nowLevel);
                //計算程式執行時間
                comTime = DateTime.Now.Ticks - startTime.Ticks;
                Console.WriteLine("總共費時{0}毫秒 耗時{1}秒", comTime / 10000, comTime / 10000000);
            }
        }

        /// <summary>
        /// 將此筆交易紀錄的路徑變化 尋覽出來 並呼叫紀錄
        /// </summary>
        static public void RecordRowProduct(int[] path, int[] productIDs, int nowLevel, int index)
        {
            int start = 0;
            if (index != 0)
            {
                start = path[index - 1] + 1;
            }

            for (int j = start; j <= productIDs.Length - 1; j++)
            {
                path[index] = j;

                if (index < nowLevel - 2)
                {
                    RecordRowProduct(path, productIDs, nowLevel, index + 1);
                }
                else
                {
                    //for (int k = 0; k < path.Length; k++)
                    //{
                    //    Console.Write(path[k]);
                    //}
                    //Console.WriteLine();

                    //取得節點
                    Product node = GetProductByLevel(nowLevel, productIDs, path);
                    if (node != null)
                    {//加入新的元素
                        for (int i = path[path.Length - 1] + 1; i < productIDs.Length; i++)
                        {
                            node.addNode(Convert.ToInt32(productIDs[i]));
                        }
                    }

                }
            }

        }

        /// <summary>
        /// 取得第幾階Product (取得節點)
        /// </summary>
        /// <param name="level">取回第幾階</param>
        /// <param name="productIDs">路徑</param>
        /// <returns></returns>
        static public Product GetProductByLevel(int level, int[] productIDs, int[] path)
        {
            Product node = null;//暫時節點
            Dictionary<int, Product> dicProduct = productRoot;//暫時組合

            //利用路path來找出正確的節點
            for (int i = 0; i < path.Length; i++)
            {
                if (!dicProduct.TryGetValue(Convert.ToInt32(productIDs[path[i]]), out node))
                {
                    return null;
                }

                if (node.nextProductNode != null)
                {
                    dicProduct = node.nextProductNode;
                }

            }
            return node;
        }

        /// <summary>
        /// 遞迴刪除小於需求數量的產品
        /// </summary>
        static public void DelProduct(Dictionary<int, Product> delProductDic, int level, int nowLevel)
        {
            if (delProductDic != null)
            {
                foreach (int overid in delProductDic.Keys.ToArray())
                {
                    //達到當前階層時進行比較、刪除
                    if (nowLevel == level)
                    {
                        //條件符合最底層的資料
                        if (delProductDic[overid].count < OVER_COUNT)
                        {
                            delProductDic.Remove(delProductDic[overid].productID);
                        }
                    }
                    else
                    {
                        //遞迴呼叫刪除往下一階層
                        DelProduct(delProductDic[overid].nextProductNode, level, nowLevel + 1);

                        //子節點如果都為0將此父節點刪除 (假設有三層，到第二層時判斷第二層的子節點，如果沒有子節點了將此節點刪除)
                        if (delProductDic[overid].nextProductNode == null)
                        {
                            delProductDic.Remove(delProductDic[overid].productID);
                        }
                        else if (delProductDic[overid].nextProductNode.Count() == 0)
                        {
                            delProductDic.Remove(delProductDic[overid].productID);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 計算此LEVEL所有組合的加總
        /// </summary>
        static public int CountFrq(int nowLevel, int maxLevel, Dictionary<int, Product> products)
        {
            int sum = 0;

            if (nowLevel == maxLevel)
            {
                return products.Count;
            }

            if (products != null)
            {
                foreach (Product product in products.Values.ToArray())
                {
                    //還沒到指定階層 繼續往下搜尋
                    if (nowLevel < maxLevel)
                    {
                        sum = sum + CountFrq(nowLevel + 1, maxLevel, product.nextProductNode);
                    }
                }
            }

            return sum;
        }
    }
}
