﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMiling
{
    public abstract class AFile
    {
        protected String path;
        protected FileStream fsSource;

        abstract public void openFile();

        abstract public void closeFile();

        abstract public int readInt();

        abstract public int[] getNextData();

    }

    


}
