﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMiling
{
    public class FileTest : AFile
    {
        int[][] testData = new int[4][];
        int index ;

        //init testData
        public FileTest(String path)
        {
            testData[0] = new int[] { 1, 3, 4 };
            testData[1] = new int[] { 2, 3, 5};
            testData[2] = new int[] { 1, 2, 3, 5 };
            testData[3] = new int[] { 2, 5 };
            index = 0;
        }

        

        public override void openFile()
        {
            index = 0;
        }

        public override void closeFile()
        {
            throw new NotImplementedException();
        }

        public override int readInt()
        {
            throw new NotImplementedException();
        }

        public override int[] getNextData()
        {
            if(index >= testData.Count())
            { 
                return null;
            }
            else
            { 
                return testData[index++];
            }
        }
    }
}
